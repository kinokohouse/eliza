# ELIZA #

The original computer therapist comes to the Macintosh command line. 

This is an Objective-C port of the original ELIZA program, and then from Steve North's Microsoft BASIC port as it appeared in Creative Computing's "More BASIC Computer Games" (MBCG for short), to be exact. It even prints the Creative Computing header as it starts up! 

A lot of care was taken to ensure this version of ELIZA behaves exactly as the Microsoft BASIC version - the example conversation in MBCG being the guide. However, some embellishments were made to the output - question marks, dots at the end of sentences, sentence capitalization etc.

To find out more about ELIZA and her history, have a look at [The Genealogy of Eliza](http://elizagen.org), which is maintained by Jeff Shrager who wrote the original BASIC version. A short 'genealogy' of sorts is also available from the program itself by entering `eliza --credits`.

We all need a bit more 'creative computing' in our lives, I believe. All work and no play... In that spirit, the one of creative computing in the 70's, the code is released under a modern public domain [unlicense](https://unlicense.org).

Ready-to-go binary for 10.7 and up is in the `Downloads` section; install in `/usr/local/bin` since that's where stuff like this belongs.

## Building

Project is for Xcode 10 and up, but you'd be able to build the one-filer `main.m` from the command line if need be.


### Version History

**1.01 (build 3, current version):**

* Unlicensed -- this information is now reflected in `--credits` option.
* Replaced `scanf` with `fgets`.
* `Hi, I'm ELIZA. What's your problem?`
