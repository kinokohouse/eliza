//
//  main.m
//  eliza
//
//  Created by Petros Loukareas on 13/07/2020.
//  Copyright © 2020 Petros Loukareas. All rights reserved.
//
//  This program was ported from the Microsoft BASIC version by
//  Steve North from 1979. In good BASIC tradition, I have
//  put some (in my case rather useless) REM-like comments in.
//

#import <Foundation/Foundation.h>

int main(int argc, const char * argv[]) {
    @autoreleasepool {
        NSString* cleanUpString(NSString *string);
        NSArray <NSString *> *k = @[ // Keywords to search for
                                    @"can you", @"can I", @"you are", @"youre", @"I dont", @"I feel",
                                    @"why don't you", @"why cant I", @"are you", @"I cant", @"I am", @"Im",
                                    @"you ", @"I want", @"what", @"how", @"who", @"where", @"when", @"why",
                                    @"name", @"cause", @"sorry", @"dream", @"hello", @"hi ", @"maybe",
                                    @" no", @"your", @"always", @"think", @"alike", @"yes", @"friend",
                                    @"computer", @"nokeyfound"
                                    ];
        NSArray <NSString *> *c = @[ // Swappable verbs
                                    @" are ", @" am ", @"were ", @"was ", @" you ", @" I ", @"your ", @"my ",
                                    @" Ive ", @" youve ", @" Im ", @" youre ",
                                    @" me ", @" !you "
                                    ];
        NSArray <NSString *> *r = @[ // Replies as format strings
                                    @"Don't you believe that I can %@?",
                                    @"Perhaps you would like to be able to %@.",
                                    @"You want me to be able to %@?",
                                    @"Perhaps you don't want to %@.",
                                    @"Do you want to be able to %@?",
                                    @"What makes you think I am %@?",
                                    @"Does it please you to believe I am %@?",
                                    @"Perhaps you would like to be %@.",
                                    @"Do you sometimes wish you were %@?",
                                    @"Don't you really %@?",
                                    @"Why don't you %@?",
                                    @"Do you wish to be able to %@?",
                                    @"Does that trouble you?",
                                    @"Tell me more about such feelings.",
                                    @"Do you often feel %@?",
                                    @"Do you enjoy feeling %@?",
                                    @"Do you really believe I don't %@?",
                                    @"Perhaps in good time I will %@.",
                                    @"Do you want me to %@?",
                                    @"Do you think you should be able to %@?",
                                    @"Why can't you %@?",
                                    @"Why are you interested in whether or not I am %@?",
                                    @"Would you prefer if I were not %@?",
                                    @"Perhaps in your fantasies I am %@.",
                                    @"How do you know you can't %@?",
                                    @"Have you tried?",
                                    @"Perhaps you can now %@.",
                                    @"Did you come to me because you are %@?",
                                    @"How long have you been %@?",
                                    @"Do you believe it is normal to be %@?",
                                    @"Do you enjoy being %@?",
                                    @"We were discussing you -- not me.",
                                    @"Oh, I %@.",
                                    @"You're not really talking about me, are you?",
                                    @"What would it mean to you if you got %@?",
                                    @"Why do you want %@?",
                                    @"Suppose you soon got %@?.",
                                    @"What if you never got %@?",
                                    @"I sometimes also want %@.",
                                    @"Why do you ask?",
                                    @"Does that question interest you?",
                                    @"What answer would please you the most?",
                                    @"What do you think?",
                                    @"Are such questions on your mind often?",
                                    @"What is it that you really want to know?",
                                    @"Have you asked anyone else?",
                                    @"Have you asked such questions before?",
                                    @"What else comes to mind when you ask that?",
                                    @"Names don't interest me.",
                                    @"I don't care about names -- please go on.",
                                    @"Is that the real reason?",
                                    @"Don't any other reasons come to mind?",
                                    @"Does that reason explain anything else?",
                                    @"What other reasons might there be?",
                                    @"Please don't apologize!",
                                    @"Apologies are not necessary.",
                                    @"What feelings do you have when you apologize?",
                                    @"Don't be so defensive!",
                                    @"What does that dream suggest to you?",
                                    @"Do you dream often?",
                                    @"What persons appear in your dreams?",
                                    @"Are you disturbed by your dreams?",
                                    @"How do you do... Please state your problem.",
                                    @"You don't seem quite certain.",
                                    @"Why the uncertain tone?",
                                    @"Can't you be more positive?",
                                    @"You aren't sure?",
                                    @"Don't you know?",
                                    @"Are you saying no just to be negative?",
                                    @"You are being a bit negative.",
                                    @"Why not?",
                                    @"Are you sure?",
                                    @"Why no?",
                                    @"Why are you concerned about my %@?",
                                    @"What about your own %@?",
                                    @"Can you think about a specific example?",
                                    @"When?",
                                    @"What are you thinking of?",
                                    @"Really, always?",
                                    @"Do you really think so?",
                                    @"But you are not sure you %@?",
                                    @"Do you doubt you %@?",
                                    @"In what way?",
                                    @"What resemblance do you see?",
                                    @"What does the similarity suggest to you?",
                                    @"What other connection do you see?",
                                    @"Could there really be some connection?",
                                    @"How?",
                                    @"You seem quite positive.",
                                    @"Are you sure?",
                                    @"I see.",
                                    @"I understand.",
                                    @"Why do you bring up the topic of friends?",
                                    @"Do your friends worry you?",
                                    @"Do your friends pick on you?",
                                    @"Are you sure you have any friends?",
                                    @"Do you impose on your friends?",
                                    @"Perhaps your love for friends worries you.",
                                    @"Do computers worry you?",
                                    @"Are you talking about me in particular?",
                                    @"Are you frightened by machines?",
                                    @"Why do you mention computers?",
                                    @"What do you think machines have to do with your problem?",
                                    @"Don't you think computers can help people?",
                                    @"What is it about machines that worries you?",
                                    @"Say, do you have any psychological problems?",
                                    @"What does that suggest to you?",
                                    @"I see.",
                                    @"I'm not sure I understand you fully.",
                                    @"Come come, elucidate your thoughts.",
                                    @"Can you elaborate on that?",
                                    @"That is quite interesting."
                                    ];
        NSArray <NSNumber *> *f = @[ // Data to link the keywords to the correct replies
                                     // These come in pairs: first item is offset (+1 as it is taken straight from
                                     // the BASIC version), second item is number of replies in category
                                    @1, @3, @4, @2, @6, @4, @6, @4, @10, @4, @14, @3, @17, @3, @20, @2, @22, @3, @25, @3,
                                    @28, @4, @28, @4, @32, @3, @35, @5, @40, @9, @40, @9, @40, @9, @40, @9, @40, @9, @40, @9,
                                    @49, @2, @51, @4, @55, @4, @59, @4, @63, @1, @63, @1, @64, @5, @69, @5, @74, @2, @76, @4,
                                    @80, @3, @83, @7, @90, @3, @93, @6, @99, @7, @106, @6
                                    ];
        NSArray <NSString *> *s = @[ // Input string sanitation -- this stuff needs to be thrown out
                                    @"-",@"'",@"`",@".",@",",@"!",@"?",@"\"",@"<",@">",@"%@"
                                    ];
        int ptr [36]; // cycle space for different replies in category, one for each keyword
        BOOL shutFlag = NO;
        NSString *p, *i, *rs, *lc;
        int j, l;
        NSInteger ofs;
        int rp, rc;
        p = @"";
        for (j = 0; j < 36; j++) {
            ptr[j] = 0;
        }
        if (argv[1] != NULL) {
            NSString *arg = [NSString stringWithUTF8String:argv[1]];
            if ([arg isEqualToString:@"--credits"]) {
                
                // Print credits and exit
                printf("\r\nCREDITS FOR ELIZA                    ObjC 1.01, build 3\r\n");
                printf("-----------------------------------------------------------\r\n");
                printf("ELIZA original SLIP program: Joseph Weizenbaum       (1966)\r\n");
                printf("Lisp port:                   Bernie Cosell    (c.1966-1972)\r\n");
                printf("Original BASIC port:         Jeff Shrager      (1973, 1977)\r\n");
                printf("Microsoft BASIC port:        Steve North             (1979)\r\n");
                printf("                       (as published in Creative Computing)\r\n");
                printf("Objective-C port:            Kinoko House            (2020)\r\n");
                printf("                   (which is based on the Steve North port)\r\n\r\n");
                printf("The source to this port is released into the public domain:\r\n");
                printf("https://bitbucket.org/kinokohouse/eliza\r\n\r\n");
            }
            return 0;
        }
        
        // Start off
        char textInput [1000];
        printf("\r\n                          ELIZA\r\n");
        printf("                    CREATIVE COMPUTING\r\n");
        printf("                  MORRISTOWN, NEW JERSEY\r\n\r\n\r\n");
        printf("Hi, I'm ELIZA. What's your problem?\r\n");
        
        // Program main loop
        do {
            printf("> ");
            fgets(textInput, sizeof(textInput), stdin);
            i = [NSString stringWithUTF8String:textInput];
            i = [[i componentsSeparatedByCharactersInSet:[NSCharacterSet newlineCharacterSet]] componentsJoinedByString:@""];
            i = [[NSString stringWithFormat:@" %@ ", i] lowercaseString];

            // Basic input sanitation, repetition check, user wants to exit
            for (j = 0; j < [s count]; j++) {
                i = [i stringByReplacingOccurrencesOfString:[s objectAtIndex:j] withString:@""];
            }
            if ([i caseInsensitiveCompare:p] == NSOrderedSame) {
                printf("Please don't repeat yourself!\r\n");
                continue;
            }
            p = i;
            if ([i rangeOfString:@"SHUT" options:NSCaseInsensitiveSearch].location != NSNotFound) {
                // end on 'shut up' or any similar variety
                shutFlag = YES;
                continue;
            }

            // get keyword from input, first keyword wins
            rs = i;
            l = -1;
            ofs = 0;
            lc = @"nokeyfound";
            for (j = 0; j < [k count]; j++) {
                NSInteger offset = [rs rangeOfString:[k objectAtIndex:j] options:NSCaseInsensitiveSearch].location;
                if (offset != NSNotFound) {
                    if (offset >= ofs) {
                        l = j;
                        ofs = offset;
                        lc = [k objectAtIndex:j];
                        break;
                    }
                }
            }
            if (l == -1) {
                l = 35;
                lc = @"nokeyfound";
            }

            // swap replaceable verbs with their counterpart
            NSString *lpart, *rpart;
            if (l != 35) {
                rs = [rs substringWithRange:NSMakeRange(ofs + [lc length], [rs length] - ofs - [lc length])];
                for (j = 0; j < [c count]; j += 2) {
                    NSString *c1 = [c objectAtIndex:j];
                    NSString *c2 = [c objectAtIndex:(j + 1)];
                    BOOL haltFlag = NO;
                    do {
                        NSRange r1 = [rs rangeOfString:c1 options:NSCaseInsensitiveSearch];
                        NSRange r2 = [rs rangeOfString:c2 options:NSCaseInsensitiveSearch];
                        if (r1.location != NSNotFound) {
                            NSInteger q = r1.location - 1;
                            if (q < 0) q = 0;
                            lpart = [rs substringWithRange:NSMakeRange(0, q + 1)];
                            rpart = [rs substringFromIndex:q + 1 + r1.length - 1];
                            rs = [NSString stringWithFormat:@" %@ %@ %@", lpart, c2, rpart];
                            if (r2.location != NSNotFound) {
                                r2.location = r2.location + ([c2 length] - [c1 length]);
                            }
                            haltFlag = NO;
                        } else {
                            haltFlag = YES;
                        }
                        if (r2.location != NSNotFound) {
                            NSInteger t = r2.location - 1;
                            if (t < 0) t = 0;
                            lpart = [rs substringWithRange:NSMakeRange(0, t)];
                            rpart = [rs substringFromIndex:t + 1 + r2.length - 1];
                            rs = [NSString stringWithFormat:@" %@ %@ %@", lpart, c1, rpart];
                            haltFlag = NO;
                        } else {
                            haltFlag = YES;
                        }
                    } while (haltFlag != YES);
                }
            }

            // Output: treat reply string as a format string if an object
            // specifier is found inside it, if not, then just output as is
            rp = [[f objectAtIndex:(l * 2)] intValue] - 1;
            rc = [[f objectAtIndex:(l * 2) + 1] intValue] - 1;
            rs = cleanUpString(rs);
            NSString *reply = [r objectAtIndex:(rp + ptr[l])];
            if ([reply rangeOfString:@"%@" options:NSCaseInsensitiveSearch].location != NSNotFound) {
                reply = [NSString stringWithFormat:reply, rs];
                reply = cleanUpString(reply);
            }
            const char *cString = [reply UTF8String];
            ptr[l]++; // bump reply cycle counter and reset if necessary
            if (ptr[l] > rc) ptr[l] = 0;
            printf("%s\r\n", cString);
        } while (!shutFlag);
        printf("Shut up...\r\n\r\n");
    }
    return 0;
}


// Sanitize output - trailing space, leading space, double spaces, etc.

NSString* cleanUpString(NSString *string) {
    if ([string length] == 0 || !string) return @"";
    while ([[string substringWithRange:NSMakeRange(0, 1)] isEqualToString:@" "]) {
        string = [string substringFromIndex:1];
        if ([string length] == 0) break;
    }
    if ([string length] == 0) return @"";
    while ([[string substringWithRange:NSMakeRange([string length] - 1, 1)] isEqualToString:@" "]) {
        string = [string substringToIndex:[string length] - 1];
        if ([string length] == 0) break;
    }
    string = [string stringByReplacingOccurrencesOfString:@"!" withString:@""];
    string = [string stringByReplacingOccurrencesOfString:@"  " withString:@" "];
    string = [string stringByReplacingOccurrencesOfString:@" ?" withString:@"?"];
    string = [string stringByReplacingOccurrencesOfString:@" ." withString:@"."];
    return string;
}
